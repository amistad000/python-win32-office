# -*- coding: utf-8 -*-
"""
Created on Thu Dec  7 13:32:47 2023

@author: User
"""
from time import sleep
import win32com.client as win32
RANGE = range(3, 8)
def word():
    word = win32.gencache.EnsureDispatch('Word.Application')
    doc = word.Documents.Open('D:\WFH\OTA\src\python\ITRI_template.docx')
    # doc = word.Documents.Add()
    word.Visible = True
    sleep(1)
    rng = doc.Range(0,0)
    rng.InsertAfter('Hacking Word with Python\r\n\r\n')
    sleep(1)
    for i in RANGE:
        rng.InsertAfter('Line %d\r\n' % i)
        sleep(1)
    rng.InsertAfter("\r\nPython rules!\r\n")
    word.ActiveDocument.SaveAs('D:\WFH\OTA\src\python\demo2.docx')
    doc.Close()
    # word.Quit()
if __name__ == '__main__':
    word()